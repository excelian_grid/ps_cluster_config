# Platform Symphony Configuration scripts #

This project is for the Nucleus Cluster Platform Symphony Configuration scripts.

## What is this repository for? ##

* roles configuration
* user configurations

## User Guide ##

### Functionality Implemented ###

***Role: roles_conf***

- add new role
- configure newly added role

***Role: users_conf***

- add new user
- configure user role

### How to run? ###

## Contributors ##
[Grzegorz Walczak](mailto:greg.walczak@excelian.com)